# Packer - Docker Base Image

This repository is used to create Docker base images used for Hetzner cloud virtual servers.

## Features

- Docker is installed
- Docker data volume is mounted during creation

## Development

Build the image:

```
$ packer build -var='hcloud_api_token=HCLOUD_API_KEY' .
```

Debug the image:

```
$ packer build -debug -var='hcloud_api_token=HCLOUD_API_KEY' .
```

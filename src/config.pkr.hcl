locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

variable "hcloud_api_token" {
  type =  string
  sensitive = true
}

source "hcloud" "default" {
  image = "debian-12"
  location = "nbg1"
  server_type = "cx22"
  snapshot_labels = {
    docker-base = true
  }
  snapshot_name = "packer-docker-base-${local.timestamp}"
  ssh_username = "root"
  token = var.hcloud_api_token
}

build {
  sources = ["source.hcloud.default"]

  provisioner "ansible" {
    extra_arguments = [ "--scp-extra-args", "'-O'" ]
    playbook_file = "./playbook.yml"
  }
}

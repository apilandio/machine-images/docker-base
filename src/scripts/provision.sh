#!/usr/bin/env bash

#
# This script is called via Terraform provisioner.
#

set -exo pipefail

echo "[PACKER] Starting server provisioning ..."

#
# Provisioning code
#

DOCKER_VOLUME_MOUNT_PATH=/mnt/docker-data

if [[ -z "${DOCKER_VOLUME_LINUX_DEVICE}" ]]; then
  echo "[PACKER] DOCKER_VOLUME_LINUX_DEVICE is missing or empty. Aborting!"
  exit 1
fi

# Mount Docker data volume
mount -o discard,defaults "${DOCKER_VOLUME_LINUX_DEVICE}" "${DOCKER_VOLUME_MOUNT_PATH}"
echo "${DOCKER_VOLUME_LINUX_DEVICE} ${DOCKER_VOLUME_MOUNT_PATH} ext4 discard,nofail,defaults 0 0" >>/etc/fstab

# Restart Docker daemon
systemctl restart docker

# Run hello-world container as integration test
docker run hello-world

#
# End provisioning code
#

echo "[PACKER] Provisioning done."

exit 0

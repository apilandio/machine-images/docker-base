#!/usr/bin/env bash

#
# This script is called via Terraform provisioner.
#

set -exo pipefail

echo "[PACKER] Starting server deprovisioning ..."

#
# Provisioning code
#

# Shutdown Docker gracefully
systemctl stop docker

#
# End provisioning code
#

echo "[PACKER] Deprovisioning done."

exit 0
